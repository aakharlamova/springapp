package ru.kharlamova.springapp.dao;

import org.springframework.jdbc.core.RowMapper;
import ru.kharlamova.springapp.models.Movie;

import java.sql.ResultSet;
import java.sql.SQLException;

public class MovieMapper implements RowMapper<Movie> {
    @Override
    public Movie mapRow(ResultSet resultSet, int i) throws SQLException {
       Movie movie = new Movie();
       movie.setId(resultSet.getInt("id"));
       movie.setTitle(resultSet.getString("title"));
       movie.setYear(resultSet.getInt("year"));
       movie.setGenre(resultSet.getString("genre"));
       movie.setWatched(resultSet.getBoolean("watched"));
       return movie;
    }

}
