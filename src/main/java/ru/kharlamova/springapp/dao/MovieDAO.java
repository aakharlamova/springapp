package ru.kharlamova.springapp.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import ru.kharlamova.springapp.models.Movie;
import java.util.List;

@Component
public class MovieDAO {
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public MovieDAO(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<Movie> index() {
        return jdbcTemplate.query("SELECT * FROM MOVIE", new MovieMapper());
    }

    public Movie show(int id) {
        return jdbcTemplate.query("SELECT * FROM MOVIE WHERE id=?", new Object[]{id}, new MovieMapper())
                .stream().findAny().orElse(null);
    }

    public void save(Movie movie) {
        jdbcTemplate.update("INSERT INTO Movie(title, year, genre, watched) VALUES(?,?,?,?)",
                movie.getTitle(), movie.getYear(), movie.getGenre(), movie.isWatched());
    }

    public void update(int id, Movie updateMovie) {
     jdbcTemplate.update("UPDATE Movie SET title=?, year=?, genre=?, watched=? WHERE id=?",
             updateMovie.getTitle(), updateMovie.getYear(), updateMovie.getGenre(), updateMovie.isWatched(), updateMovie.getId());
    }

    public void delete(int id) {
        jdbcTemplate.update("DELETE FROM Movie WHERE id=?", id);
    }

}
