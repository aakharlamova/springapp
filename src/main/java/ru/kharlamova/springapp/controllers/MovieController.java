package ru.kharlamova.springapp.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.kharlamova.springapp.dao.MovieDAO;
import ru.kharlamova.springapp.models.Movie;

@Controller
@RequestMapping("/movies")
public class MovieController {

    private final MovieDAO movieDAO;

    @Autowired
    public MovieController(MovieDAO movieDAO) {
        this.movieDAO = movieDAO;
    }

    @GetMapping()
    public String index(Model model) {
        model.addAttribute("movies", movieDAO.index());
        return "movies/index";
    }

    @GetMapping("/{id}")
    public String show(@PathVariable("id") int id, Model model) {
        model.addAttribute("movie", movieDAO.show(id));
        return "movies/show";
    }

    @GetMapping("/new")
    public String newPerson(@ModelAttribute("movie") Movie person) {
        return "movies/new";
    }

    @PostMapping()
    public String create(@ModelAttribute("movie") Movie person) {
        movieDAO.save(person);
        return "redirect:/movies";
    }

    @GetMapping("/{id}/edit")
    public String edit(Model model, @PathVariable("id") int id) {
        model.addAttribute("movie", movieDAO.show(id));
        return "movies/edit";
    }

    @PatchMapping("/{id}")
    public String update(@ModelAttribute("movie") Movie person,
                         @PathVariable("id") int id) {
        movieDAO.update(id, person);
        return "redirect:/movies";
    }

    @DeleteMapping("/{id}")
    public String delete(@PathVariable("id") int id) {
        movieDAO.delete(id);
        return "redirect:/movies";
    }

}
